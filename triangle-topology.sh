#!/bin/bash

#
# Copyright (c) 2017 Stanislav Galfy
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# - The name of the author may not be used to endorse or promote products
#   derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


killall vde_switch

vde_switch -s /tmp/in_switch1 -d
vde_switch -s /tmp/in_switch2 -d
vde_switch -s /tmp/in_switch3 -d
vde_switch -s /tmp/helenos_switch1 -d
vde_switch -s /tmp/helenos_switch2 -d
vde_switch -s /tmp/core1_switch1 -d
vde_switch -s /tmp/core1_switch2 -d
vde_switch -s /tmp/core2_switch1 -d
vde_switch -s /tmp/core2_switch2 -d

qemu-system-i386 client.img \
	-net nic,vlan=01,macaddr=ca:fe:ba:be:00:01 \
	-net vde,vlan=01,sock=/tmp/helenos_switch1 \
	-machine type=pc,accel=kvm -m 256 \
	-name client &

qemu-system-i386 -cdrom ../mainline/image.iso \
	-net nic,vlan=11,macaddr=ca:fe:ba:be:01:01,model=rtl8139 \
	-net vde,vlan=11,sock=/tmp/in_switch1 \
	-net nic,vlan=12,macaddr=ca:fe:ba:be:01:02,model=rtl8139 \
	-net vde,vlan=12,sock=/tmp/in_switch2 \
	-net nic,vlan=13,macaddr=ca:fe:ba:be:01:03,model=rtl8139 \
	-net vde,vlan=13,sock=/tmp/helenos_switch1 \
	-net nic,vlan=14,macaddr=ca:fe:ba:be:01:04,model=rtl8139 \
	-net vde,vlan=14,sock=/tmp/helenos_switch2 \
	-machine type=pc,accel=kvm -m 256 \
	-name helenos &
	
qemu-system-i386 core_1.img \
	-net nic,vlan=21,macaddr=ca:fe:ba:be:02:01 \
	-net vde,vlan=21,sock=/tmp/in_switch1 \
	-net nic,vlan=22,macaddr=ca:fe:ba:be:02:02 \
	-net vde,vlan=22,sock=/tmp/in_switch3 \
	-net nic,vlan=23,macaddr=ca:fe:ba:be:02:03 \
	-net vde,vlan=23,sock=/tmp/core1_switch1 \
	-net nic,vlan=24,macaddr=ca:fe:ba:be:02:04 \
	-net vde,vlan=24,sock=/tmp/core1_switch2 \
	-machine type=pc,accel=kvm -m 256 \
	-name core_1 &

qemu-system-i386 core_2.img \
	-net nic,vlan=31,macaddr=ca:fe:ba:be:03:01 \
	-net vde,vlan=31,sock=/tmp/in_switch2 \
	-net nic,vlan=32,macaddr=ca:fe:ba:be:03:02 \
	-net vde,vlan=32,sock=/tmp/in_switch3 \
	-net nic,vlan=33,macaddr=ca:fe:ba:be:03:03 \
	-net vde,vlan=33,sock=/tmp/core2_switch1 \
	-net nic,vlan=34,macaddr=ca:fe:ba:be:03:04 \
	-net vde,vlan=34,sock=/tmp/core2_switch2 \
	-machine type=pc,accel=kvm -m 256 \
	-name core_2
	

while true
do
sleep 1
done
